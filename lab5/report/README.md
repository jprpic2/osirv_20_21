# Lab 5 - Report

### Problem 1

```python
frame = cv2.flip(frame,1)
```

U navedenoj liniji koda 2. parametar funkcije predstavlja smjer okretanja slike.
<ul>
	<li> >0 okreće horizontalno
	<li> 0 okreće vertikalno
	<li> <0 okreće u oba smjera
</ul>


### Problem 2

Razlika između sparse i dense optical flow metoda je da sparse (Lucas-Kanade Method) uzima rubove određene minimizacijom i iscrtava njihovu putanju u odnosu na subjekt.
Dense optical flow metoda iscrtava sve teksture koje se gibaju različito od subjekta.

### Problem 3

Snimanje videa kamerom ostvaruje se stavljanjem 0 umjesto imena datoteke videa.