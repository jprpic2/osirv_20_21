# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 12:06:48 2020

@author: Jakov-PC
"""

import numpy as np
import cv2
import os

def load_images_from_folder(folder):
    images = []
    imageNames=[]
    for filename in os.listdir(folder):
        imageNames.append(filename[:filename.index(".")])
        img = cv2.imread(os.path.join(folder,filename),0)
        if img is not None:
            images.append(img)
    return images, imageNames

folder=r"..\..\slike"

images,imageNames=load_images_from_folder(folder)

images=np.array(images)
for i in range(images.size):
    images[i]=~images[i]
    cv2.imshow(imageNames[i]+'_invert.jpg',images[i])
    cv2.imwrite(imageNames[i]+'_invert.jpg',images[i])

cv2.waitKey(0)
cv2.destroyAllWindows()
 