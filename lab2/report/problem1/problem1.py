# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 14:45:17 2020

@author: Jakov-PC
"""
import numpy as np
import cv2

def addImageBorder(image, borderSize):
    borderSize=int(borderSize/2)
    image=np.pad(image,pad_width=((borderSize,borderSize),(borderSize,borderSize),(0,0)),mode='constant',constant_values=0) 
    return image


slika_1 = cv2.imread(r"..\..\slike\baboon.bmp")
slika_2 = cv2.imread(r"..\..\slike\airplane.bmp")
slika_3 = cv2.imread(r"..\..\slike\barbara.bmp")

rows,cols=slika_1[:,:,0].shape

slika_2_cut = slika_2[0:rows,0:cols,:]
slika_3_cut = slika_3[0:rows,0:cols,:]

slika= np.hstack((slika_1,slika_2_cut,slika_3_cut))
image=addImageBorder(slika,20)
cv2.imshow('slika',image)
cv2.imwrite('merged_bordered.jpg',image)

cv2.waitKey(0)
cv2.destroyAllWindows()
