# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:00:22 2020

@author: Jakov-PC
"""
import numpy as np
import cv2

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]
    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    
    return output

slika= cv2.imread(r"..\..\slike\airplane.bmp")
gray_slika = cv2.imread(r"..\..\slike\airplane.bmp",0)

#Kerneli
edgeDetection=np.array([[-1,-1,-1],[-1,8,-1],[-1,-1,-1]])
sharpen=np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])
boxBlur=np.ones((3,3))*(1/9)
#Arrayevi za nove slike
modifiedSlika=np.empty((slika.shape[0]-2,slika.shape[1]-2,3)).astype(np.uint8)
graySlika=np.empty((slika.shape[0]-2,slika.shape[1]-2)).astype(np.uint8)

cv2.imshow('slika.jpg',slika) 
#Konvolucija
modifiedSlika[:,:,0]=convolve(slika[:,:,0],sharpen)
modifiedSlika[:,:,1]=convolve(slika[:,:,1],sharpen)
modifiedSlika[:,:,2]=convolve(slika[:,:,2],sharpen)
graySlika=convolve(gray_slika,edgeDetection)


cv2.imshow('modifiedSlika.jpg',modifiedSlika) 
cv2.imshow('graySlika.jpg',graySlika)

cv2.imwrite('modifiedSlika.jpg',modifiedSlika) 
cv2.imwrite('edgeDetection.jpg',graySlika)

cv2.waitKey(0)
cv2.destroyAllWindows()



