# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 12:30:57 2020

@author: Jakov-PC
"""

import numpy as np
import cv2
import os

def load_images_from_folder(folder):
    images = []
    imageNames=[]
    for filename in os.listdir(folder):
        imageNames.append(filename[:filename.index(".")])
        img = cv2.imread(os.path.join(folder,filename),0)
        if img is not None:
            images.append(img)
    return images, imageNames

def threshold(images,threshold_value):
    for i in range(images.size):
        images[i][images[i]<=threshold_value]=0
        cv2.imwrite(imageNames[i]+'_threshold_'+str(threshold_value)+'.jpg',images[i])

folder=r"..\..\slike"

images,imageNames=load_images_from_folder(folder)
images=np.array(images)

threshold(images,63)
threshold(images,127)
threshold(images,191)

cv2.waitKey(0)
cv2.destroyAllWindows()
 