# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 13:43:16 2020

@author: Jakov-PC
"""

import numpy as np
import cv2
def quantise(image, q):
    d=2**(8-q)
    quantised_image=(((np.floor(image.astype(np.float32)/d))+1/2)*d).astype(np.uint8)
    return quantised_image

slika = cv2.imread(r"..\..\slike\BoatsColor.bmp",0)

for i in range(1,9):
    cv2.imwrite("boats_"+str(i)+".bmp",quantise(slika,i))

cv2.waitKey(0)
cv2.destroyAllWindows()