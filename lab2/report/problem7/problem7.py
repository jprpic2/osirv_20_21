# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 13:08:23 2020

@author: Jakov-PC
"""
import cv2
import numpy as np

def show(img):
  cv2.imshow("title", img)
  cv2.moveWindow("title", 800, 200)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

img = cv2.imread(r'..\..\slike\baboon.bmp', 0)
img = cv2.resize(img, (0,0), fx=0.25, fy=0.25) 
rows,cols = img.shape


images=[]
for i in range(0,12):
     M = cv2.getRotationMatrix2D((cols/2,rows/2),i*30,1)
     slika=cv2.warpAffine(img,M,(cols,rows))
     images.append(slika)
     
images=np.hstack(images)
show(images)
cv2.imwrite("Rotation.jpg", images)






