# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 14:01:08 2020

@author: Jakov-PC
"""

import numpy as np
import cv2
def quantise(image, q, n):
    d=2**(8-q)
    quantised_image=(((np.floor(image.astype(np.float32)/d + n))+1/2)*d).astype(np.uint8)
    return quantised_image

slika = cv2.imread(r"..\..\slike\BoatsColor.bmp",0)
n=np.random.uniform(low=0,high=1,size=slika.shape)
for i in range(1,9):
    cv2.imwrite("boats_"+str(i)+"n.bmp",quantise(slika,i,n))


cv2.waitKey(0)
cv2.destroyAllWindows()