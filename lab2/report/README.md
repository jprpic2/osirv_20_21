# Izvješće

## Problem 1

Učitavanjem slika dobije se matrica s 3 kanala. Funckija np.pad dodaje nove stupce i retke u svaku dimenziju slike, te kako bi se zadržala 3 kanala slike, 3. argument pad_width postavlja na (0,0).
Dodavanje bordera na sliku obavlja se funkcijom:

```python
def addImageBorder(image, borderSize):
    borderSize=int(borderSize/2)
    image=np.pad(image,pad_width=((borderSize,borderSize),(borderSize,borderSize),(0,0)),mode='constant',constant_values=0) 
    return image
``` 

Kako bi se slike dodale jedna pored druge, treba uskladiti dimenzije na temelju prve učitane slike:

```python
rows,cols=slika_1[:,:,0].shape

slika_2_cut = slika_2[0:rows,0:cols,:]
slika_3_cut = slika_3[0:rows,0:cols,:]

slika= np.hstack((slika_1,slika_2_cut,slika_3_cut))
```

Na kraju dobivamo borderani skup tri učitane slike:

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem1/merged_bordered.jpg "Problem 1 slika")

## Problem 2 - Konvolucija

Zadana funkcija konvolucije mora se provući kroz sva 3 kanala rgb slike ili kroz jedan kanal grayscale slike.
Kreiranje kernela te unošenje argumenata u funkciju s predloška prikazano je kodom:

```python
#Kerneli
edgeDetection=np.array([[-1,-1,-1],[-1,8,-1],[-1,-1,-1]])
sharpen=np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])
boxBlur=np.ones((3,3))*(1/9)
#Arrayevi za nove slike
modifiedSlika=np.empty((slika.shape[0]-2,slika.shape[1]-2,3)).astype(np.uint8)
graySlika=np.empty((slika.shape[0]-2,slika.shape[1]-2)).astype(np.uint8)

#Konvolucija
modifiedSlika[:,:,0]=convolve(slika[:,:,0],sharpen)
modifiedSlika[:,:,1]=convolve(slika[:,:,1],sharpen)
modifiedSlika[:,:,2]=convolve(slika[:,:,2],sharpen)
graySlika=convolve(gray_slika,edgeDetection)
```

Nakon prikazanog koda stvaraju se slike: 

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/slike/airplane.bmp "Originalna slika")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem2/modifiedSlika.jpg "Problem 2 sharpened slika")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem2/edgeDetection.jpg "Problem 2 edgeDetection slika")

## Problem 3 - Invert

Funkcija za dohvaćanje slika i njihovih imena iz direktorija prikazano je kodom:

```python
def load_images_from_folder(folder):
    images = []
    imageNames=[]
    for filename in os.listdir(folder):
        imageNames.append(filename[:filename.index(".")])
        img = cv2.imread(os.path.join(folder,filename),0)
        if img is not None:
            images.append(img)
    return images, imageNames
```

Nakon dohvaćanja slika, prikazuju se i spremaju na sljedeći način:
```python
folder=r"..\..\slike"

images,imageNames=load_images_from_folder(folder)

images=np.array(images)
for i in range(images.size):
    images[i]=~images[i]
    cv2.imshow(imageNames[i]+'_invert.jpg',images[i])
    cv2.imwrite(imageNames[i]+'_invert.jpg',images[i])
```

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem3/airplane_invert.jpg "Problem 3 invertirana slika")

## Problem 4 - Threshold

Dohvaćanje slika iz direktorija obavljeno je na isti način kao i u prethodnom zadatku.
Svaki piksel kojem je vrijednost manja od threshold vrijednosti se izjednačuje s 0. Tako izmjenjena slika se sprema.
Funkcija koja to radi prikazana je kodom:
```python
def threshold(images,threshold_value):
    for i in range(images.size):
        images[i][images[i]<=threshold_value]=0
        cv2.imwrite(imageNames[i]+'_threshold_'+str(threshold_value)+'.jpg',images[i])
```
Učitavanje slika i pozivanje funkcija se obavlja na sljedeći način:

```python
images,imageNames=load_images_from_folder(folder)
images=np.array(images)

threshold(images,63)
threshold(images,127)
threshold(images,191)
```

Za svaku sliku iz direktorija dobivamo tri verzije s odgovarajućim thresholdom:

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem4/airplane_threshold_63.jpg "Problem 4 threshold=63")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem4/airplane_threshold_127.jpg "Problem 4 threshold=127")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem4/airplane_threshold_191.jpg "Problem 4 threshold=191")

## Problem 5 - Quantisation

Kvantizacija slika ostvarena je funkcijom quantise:

```python
def quantise(image, q):
    d=2**(8-q)
    quantised_image=(((np.floor(image.astype(np.float32)/d))+1/2)*d).astype(np.uint8)
    return quantised_image
```

Kvantizacijom se dobivaju slike u kojima je korišteno $`2^q`$ nijansi sive boje. 
Prikazane su slike gdje je q=1,2,3. Više slika je u direktoriju problem5.

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem5/boats_1.bmp "Problem 5 q=1")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem5/boats_2.bmp "Problem 5 q=2")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem5/boats_3.bmp "Problem 5 q=3")

Nakon 6. potencije broja 2 prestajemo vidjeti razliku među slikama iz razloga što ljudi vide samo 40 nijansi sive boje.

## Problem 6 - Noise

Kvantizacija s šumom ostvarena je funkcijom:

```python
def quantise(image, q, n):
    d=2**(8-q)
    quantised_image=(((np.floor(image.astype(np.float32)/d + n))+1/2)*d).astype(np.uint8)
    return quantised_image
```

gdje je n realiziran uniformnom razdiobom prema predlošku `n = np.random.uniform(0,1, shape )`.

Slike s uvedenim šumom:

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem6/boats_1n.bmp "Problem 6 q=1")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem6/boats_2n.bmp "Problem 6 q=2")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem6/boats_3n.bmp "Problem 6 q=3")

Na slikama se koriste 1.,2. i 3. potencija broja 2. Više slika je u direktoriju problem6.
Što se više nijansi sive boje koristi, to se manje šum vidi.

## Problem 7 - Geometric transformations of Images

Problem je bio skalirati sliku na četvrtinu njene originalne veličine, rotirati ju 12 puta za 30° te svaku
iteraciju prikazati na istoj slici.
Nakon skaliranja originalne slike, svaka se iteracija rotiranja slike sprema u listu. Ta lista se predaje kao
argument funkciji hstack kako bi se dobila jedna slika s horizontalno poredanim iteracijama rotiranja slike.

```python
img = cv2.imread(r'..\..\slike\baboon.bmp', 0)
img = cv2.resize(img, (0,0), fx=0.25, fy=0.25) 
rows,cols = img.shape


images=[]
for i in range(0,12):
     M = cv2.getRotationMatrix2D((cols/2,rows/2),i*30,1)
     slika=cv2.warpAffine(img,M,(cols,rows))
     images.append(slika)
     
images=np.hstack(images)
show(images)
```

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab2/report/problem7/Rotation.jpg "Problem 7 rotation")
