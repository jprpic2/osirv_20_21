# -*- coding: utf-8 -*-
"""
Created on Sun Dec 13 15:40:16 2020

@author: Jakov-PC
"""

import numpy as np
import cv2

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def median_filter(img,radius):
    out=img.astype(np.float32)
    distance=int(radius/2)
    for i in range(distance,out.shape[0]):
            for j in range(distance,out.shape[1]):
                median=np.sort(out[i-distance:i+distance+1,j-distance:j+distance+1].reshape(1,-1))
                shape=median.shape[1]/2
                if(shape%2==0):
                    value=np.average(median[0,int(shape)-1:int(shape)+1])
                else:
                    value=median[0,int(shape)]
                out[i,j]=value
    return out.astype(np.uint8)

boats=cv2.imread(r"..\..\slike\boats.bmp",0)
airplane=cv2.imread(r"..\..\slike\airplane.bmp",0)

boats_snp_1=salt_n_pepper_noise(boats,1)
boats_snp_10=salt_n_pepper_noise(boats,10)

airplane_snp_1=salt_n_pepper_noise(airplane,1)
airplane_snp_10=salt_n_pepper_noise(airplane,10)

boats_gauss_5=gaussian_noise(boats,0,5)
boats_gauss_15=gaussian_noise(boats, 0, 15)
boats_gauss_35=gaussian_noise(boats, 0, 35)

airplane_gauss_5=gaussian_noise(airplane, 0, 5)
airplane_gauss_15=gaussian_noise(airplane, 0, 15)
airplane_gauss_35=gaussian_noise(airplane, 0, 35)

median = cv2.medianBlur( airplane_snp_10, 3 )
blur = cv2.GaussianBlur( airplane_snp_10, (3, 3), 3 )

cv2.imshow("original",boats)
cv2.imshow("median",median)
cv2.imshow("MyFilter",median_filter(airplane_snp_10, 3))

cv2.waitKey(0)
cv2.destroyAllWindows()