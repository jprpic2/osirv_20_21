# Izvješće

## Problem 1

Na sliku **`slike/airplane.bmp`** dodani su Gaussian, uniformni i Salt and pepper šumovi po zadanim parametrima.
Kako bi se razlika u histogramima što više primjetila, sljedeće slike su one s najvećim parametrima šuma. 

Prva slika je histogram originalne slike kao histogram za usporedbu.

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem1/airplane_hist.jpg "Problem 1 original")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem1/gaussian_noise_20_hist.jpg "Problem 1 Gaussian noise 20")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem1/salt_n_pepper_noise_20_hist.jpg "Problem 1 Salt and pepper 20")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem1/uniform_noise_60_hist.jpg "Problem 1 Uniform noise 60")

U histogramu slike na koju je dodan Gaussian šum, vidljivo je kako je originalni histogram izgubio oštrinu i vrhovi su više poput Gaussove krivulje.
Salt and pepper histogram se razlikuje po tome što je količina vrijednosti 0 i 255 veća svakim povećanjem parametra šuma. Uniformni šum smanjuje vrhove histograma i čini ga ravnijim.

## Problem 2

Na sliku **`slike/airplane.bmp`** dodani su šumovi salt and pepper i Gaussian. Nakon dodavanja šuma primjenjena su Gauss i median zamućenja.

Na sljedećim slikama prikazan je Gaussian šum. Na prvu sliku dodano je Gaussovo zamućenje, dok je na drugu sliku median zamućenje.

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem2/gauss_blur_gauss_0.jpg "Problem 2 Gauss noise, Gauss blur")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem2/median_blur_gauss_3.jpg "Problem 2 Gauss noise, median blur")

Po slovima možemo vidjeti kako nakon Gaussovog zamućenja je text na avionu čitljiviji nego nakon dodavanja median zamućenja.

Na sljedeće slike je dodan salt and pepper šum. 

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem2/gauss_blur_snp_0.jpg  "Problem 2 S&P noise, Gauss blur")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem2/median_blur_snp_3.jpg "Problem 2 S&P noise, median blur")

Gaussovo zamućenje nije nimalo pomoglo, dok median zamućenje uklanja 99.9% šuma.

### Median filter

Sljedeći zadatak bio je napisati svoju funkciju za dodavanje median zamućenja. 
Median zamućenje funkcionira na temelju traženja srednje vrijednosti susjednih vrijednosti podmatrice određene radijusom. 
Nakon pronalaska srednje vrijednosti, trenutni piksel se izjednačuje s njom. Sljedeći kod obavlja opisanu funkciju.

```python
def median_filter(img,radius):
    out=img.astype(np.float32)
    distance=int(radius/2)
    for i in range(distance,out.shape[0]):
            for j in range(distance,out.shape[1]):
                median=np.sort(out[i-distance:i+distance+1,j-distance:j+distance+1].reshape(1,-1))
                shape=median.shape[1]/2
                if(shape%2==0):
                    value=np.average(median[0,int(shape)-1:int(shape)+1])
                else:
                    value=median[0,int(shape)]
                out[i,j]=value
    return out.astype(np.uint8)
```

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem2/CV2Filter.jpg "Problem 2 cv2 filter")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem2/MyFilter.jpg "Problem 2 median_filter output")

Slikama je prikazana razlika rezultantnih slika cv2 funkcije median filtera i napisane **`median_filter`** funkcije.
Funkcija je uspjela otkloniti salt and pepper šum, kvaliteta je otprilike ista, no treba joj duže da se izvrši.

## Problem 3

### Adaptive Thresholding

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem3/Comparison_airplane_35.png "Problem 3 Adaptive Thresholding airplane")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem3/Comparison_adaptive_boats_11.png "Problem 3 Adaptive Thresholding boats")

Adaptive Thresholding ostavlja puno više rubova nego jednostavni thresholding. Uz to, median više ističe rubove nego Gaussian thresholding.


### Otsu’s Binarization

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem3/Comparison_Otsu_airplane.png "Problem 3 Otsu’s Binarization airplane")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem3/Comparison_Otsu_boats.png "Problem 3 Otsu’s Binarization boats")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab3/report/problem3/Comparison_Otsu_baboon.png "Problem 3 Otsu’s Binarization baboon")

Na primjerima vidimo koliko Otsu binarizacija bolje bira vrijednost thresholdinga, čak i kad slika nije bimodalna. Također, dok Otsu binarizacija automatski izračuna tu vrijednost, nama bi trebalo nekoliko pokušaja da odredimo koja je najbolja vrijednost.