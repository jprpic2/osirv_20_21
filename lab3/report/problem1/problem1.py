# -*- coding: utf-8 -*-
"""
Created on Sun Dec 13 14:28:39 2020

@author: Jakov-PC
"""
import numpy as np
import matplotlib.pyplot as plt
import cv2

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img,imgName):
  cv2.imshow("title", img)
  cv2.imwrite(imgName+".jpg",img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
def showhist(img,imgName):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.savefig(imgName+"_hist.jpg")
  plt.show()
  
img=cv2.imread(r"..\..\slike\airplane.bmp",0)


show(img,"airplane")
showhist(img,"airplane")

# show(salt_n_pepper_noise(img,5),"salt_n_pepper_noise_5")
# showhist(salt_n_pepper_noise(img,5),"salt_n_pepper_noise_5")

# show(salt_n_pepper_noise(img,10),"salt_n_pepper_noise_10")
# showhist(salt_n_pepper_noise(img,10),"salt_n_pepper_noise_10")

# show(salt_n_pepper_noise(img,15),"salt_n_pepper_noise_15")
# showhist(salt_n_pepper_noise(img,15),"salt_n_pepper_noise_15")

# show(salt_n_pepper_noise(img,20),"salt_n_pepper_noise_20")
# showhist(salt_n_pepper_noise(img,20),"salt_n_pepper_noise_20")

# show(gaussian_noise(img, 0, 5),"gaussian_noise_5")
# showhist(gaussian_noise(img, 0, 5),"gaussian_noise_5")
# show(gaussian_noise(img, 0, 10),"gaussian_noise_10")
# showhist(gaussian_noise(img, 0, 10),"gaussian_noise_10")
# show(gaussian_noise(img, 0, 15),"gaussian_noise_15")
# showhist(gaussian_noise(img, 0, 15),"gaussian_noise_15")
# show(gaussian_noise(img, 0, 20),"gaussian_noise_20")
# showhist(gaussian_noise(img, 0, 20),"gaussian_noise_20")

# show(uniform_noise(img, -20, 20),"uniform_noise_20")
# showhist(uniform_noise(img, -20, 20),"uniform_noise_20")
# show(uniform_noise(img, -40, 40),"uniform_noise_40")
# showhist(uniform_noise(img, -40, 40),"uniform_noise_40")
# show(uniform_noise(img, -60, 60),"uniform_noise_60")
# showhist(uniform_noise(img, -60, 60),"uniform_noise_60")
