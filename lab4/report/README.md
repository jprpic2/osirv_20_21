# Lab 4 - Report

### Problem 1 - Edge Detection with Canny Edge descriptor

Kod iz predloška primjenjen je na sliku *`sudoku.jpg`*. Korišteni su parametri
<ul>
	<li>lower=0, upper=255</li>
	<li>lower=128, upper=128</li>
	<li>lower=255, upper=255</li>
	<li>lower=127, upper=255</li>
</ul>

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem1/low_0_high_255.JPG "Problem 1, lower=0, upper=255")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem1/low_127_high_255.JPG "Problem 1, lower=128, upper=128")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem1/low_255_high_255.JPG "Problem 1, lower=255, upper=255")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem1/low_127_high_255.JPG "Problem 1, lower=127, upper=255")

Najbolji su rezultati kada su parametri bili lower=127 i upper 255. Kroz svaku varijaciju parametara vide se brojevi, no na 3. slici vidljivo je kako se grid gubi, tj prestaje se prepoznavati kao edge.

### Problem 2 - Automatic Canny Edge detection

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem2/lenna.png "Problem 2, lenna")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem2/pepper.png "Problem 2, pepper")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem2/airplane.png "Problem 2, airplane")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem2/barbara.png "Problem 2, barbara")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem2/boats.png "Problem 2, boats")

Za prikazane slike dobiveni su vrijednosti:
<ul>
	<li>Lenna - Median lower: 83, Median upper: 166</li>
	<li>Pepper - Median lower: 81, Median upper: 160</li>
	<li>Airplane - Median lower: 134, Median upper: 255</li>
	<li>Barbara - Median lower: 72, Median upper: 143</li>
	<li>Boats - Median lower: 92, Median upper: 183</li>
</ul>

### Problem 3 - Hough Transformation for Lines


Kod iz predloška primjenjen je na sliku *`chess.jpg`* uz parametre:

<ul>
	<li>Theta:90, Threshold: 150</li>
	<li>Theta:180, Threshold: 200</li>
	<li>Theta:90, Threshold: 200</li>
	<li>Theta:180, Threshold: 150</li>
</ul>

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem3/theta_90_thresh_150.png "Problem 3, theta_90_thresh_150")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem3/theta_180_thresh_200.png "Problem 3, theta_180_thresh_200")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem3/theta_90_thresh_200.png "Problem 3, theta_90_thresh_200")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem3/theta_180_thresh_150.png "Problem 3, theta_180_thresh_150")

Iz slika je vidljivo da se uz vrijednosti theta 180, threshold 200 najbolje prepoznaju linije na šahovskoj ploči. Uz threshold 150, previše se linija prikazuje tj. neke se prikazuju duplo uz odstupanje.

### Problem 4 - Corner Detection

Kod iz predloška primjenjen je na sliku *`chess.jpg`* uz parametre:

<ul>
	<li>blockSize: 1</li>
	<li>blockSize: 3</li>
	<li>blockSize: 5</li>
</ul>

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem4/blockSize_1.jpg "Problem 4, blockSize_1")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem4/blockSize_3.jpg "Problem 4, blockSize_3")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem4/blockSize_5.jpg "Problem 4, blockSize_5")

Iz slika je vidljivo da blockSize = 1 ne prepoznaje vrhove, dok blockSize = 5 prepoznaje previše.

### Problem 5 - Feature Detection with ORB detector

Kod iz predloška primjenjen je na slike *`roma_1.jpg`* i *`roma_2.jpg`*. Prikazane su rezultatne slike.

![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem5/roma_ORB.jpg "Problem 5, roma_ORB")
![alt text](https://gitlab.com/jprpic2/osirv_20_21/-/raw/master/lab4/report/problem5/roma_Detector.jpg "Problem 5, roma_Detector")